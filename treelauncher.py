#-------------------------------------------------------------------------------
# Name:        treelauncher.py
# Purpose:
#
# Author:      alexey.vinogradov
#
# Created:     17.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from sets import Set
import sys

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from geo import *
import skeleton

# Global skeleton and polygon instances for rendering
gSkeleton   = []
gPolygon    = []
# Polygon extents, [[min x, min y], [max x, max y]]
gPolyExtent = []

emin = 0
emax = 1

CHAR_ASCENT  = 119.05
CHAR_DECENT  = 33.3
CHAR_HEIGHT  = CHAR_ASCENT + CHAR_DECENT
CHAR_UNIFORM = 1.0
TEXT_OFFSET  = 0.1

#-------------------------------------------------------------------------------
# OpenGL visualisation part
#-------------------------------------------------------------------------------
def initGl():
    global gPolyExtent

    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)

    glEnable    (GL_DEPTH_TEST)

    gPolyExtent = [[gPolygon[ppnts][0][px], gPolygon[ppnts][0][py]],
                   [gPolygon[ppnts][0][px], gPolygon[ppnts][0][py]] ]
    # Calculate extents of the polygon
    for point in gPolygon[ppnts]:
        for i in range(px, py + 1):
            if point[i] < gPolyExtent[emin][i]:
                gPolyExtent[emin][i] = point[i]
            elif point[i] > gPolyExtent[emax][i]:
                gPolyExtent[emax][i] = point[i]

def reshapeGl(width, height):
    global gPolyExtent

    near       = 0.0
    far        = 1.0
    vpOffset   = 0.3
    projOffset = 0.5

    xmin = gPolyExtent[emin][px]
    ymin = gPolyExtent[emin][py]
    xmax = gPolyExtent[emax][px]
    ymax = gPolyExtent[emax][py]

    vheight = max(height, (ymax - ymin + vpOffset) / (xmax - xmin + vpOffset))

    glViewport     (0, 0, width, vheight)
    glMatrixMode   (GL_PROJECTION)
    glLoadIdentity ()
    glOrtho(xmin - projOffset, xmax + projOffset, ymin - projOffset, ymax + projOffset, near, far)
    glMatrixMode   (GL_MODELVIEW)
    glLoadIdentity ()

def drawTextLine(text, position, scale, color):
    glPushMatrix()

    glColor3d   (color[0], color[1], color[2])

    glTranslated(position[0], position[1], position[2])
    glScaled    (scale, scale, scale)

    map(lambda c: glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, ord(c)), text)

    glPopMatrix ()

def drawText(text, position, scale, color):
    yBase = position[1]
    for line in text:
        drawTextLine(line, [position[0], yBase, position[2]], scale, color)
        yBase += CHAR_UNIFORM

def drawSkeleton(skeleton, color):
    def drawEdge(edge):
        glVertex2d(edge[0][px], edge[0][py])
        glVertex2d(edge[1][px], edge[1][py])
    def drawTextPoint(p):
        position  = [p[px], p[py] + TEXT_OFFSET, 0.0]
        scale     = 1.0 / (CHAR_HEIGHT * 20)
        textColor = (1.0, 0.0, 0.0)
        drawTextLine('(%.2f, %.2f)' % (p[px], p[py]), position, scale, textColor)

    def drawText(edge):
        drawTextPoint(edge[0])
        drawTextPoint(edge[1])


    map(lambda edge: drawText(edge), skeleton)

    # Draw edges
    glColor3d(color[0], color[1], color[2])

    glBegin  (GL_LINES)

    map(lambda edge: drawEdge(edge), skeleton)

    glEnd    ()

    glPointSize(6.0)
    glBegin  (GL_POINTS)

    map(lambda edge: drawEdge(edge), skeleton)

    glEnd    ()


def displayGl():
    global gPolygon
    global gSkeleton

    glClear        (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glPushMatrix   ()

    drawSkeleton   (gSkeleton, (1.0, 1.0, 1.0))

    glPopMatrix    ()

    glutSwapBuffers()

def idleGl():
    glutPostRedisplay()

def keyPressed(*args):
    # Exit, when ESC was pressed
    if args[0] == '\033':
        sys.exit()

#
# Read polygon input data from the file with given name
# Takes O(N) time, where N is the number of polygon points
#
def readPoly(fileName):
    with open(fileName) as f:
        # Obtain the list of points, but don't validate the input.
        # If you're wrong, wait for the exception here!
        pts = [(float(line.replace('\n', '').split(' ')[px]), float(line.replace('\n', '').split(' ')[py])) for line in f.readlines()]

        n = len(pts)
        # Setup polygon edges, see above (dir, pivot)
        edges = [((pminus(pts[(i + 1) % n], pts[i])), pts[i]) for i in range(0, n)]

        # Poly is ready...
        return (pts, edges)



#
# Write skeleton into the file with given name
# Takes O(N) time
#
def displaySkeleton(skeleton, poly):
    global gSkeleton
    global gPolygon

    gSkeleton = skeleton
    gPolygon  = poly

    glutInit               (sys.argv)
    glutInitDisplayMode    (GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
    glutInitWindowSize     (1024, 768)

    glutCreateWindow ('Straight skeleton demonstration')
    glutDisplayFunc  (displayGl )
    glutIdleFunc     (initGl    )
    glutReshapeFunc  (reshapeGl )
    glutKeyboardFunc (keyPressed)
    initGl           ()

    print "Hit ESC key to quit."

    glutMainLoop()

def dumpSkeletonGraph(fileName, ss):
    V = Set()
    for edge in ss:
        V.add(edge[0])
        V.add(edge[1])

    E = Set(ss)

    vertexPattern = '(%s, %s)\n'
    edgePattern   = '[(%s, %s) (%s, %s)]\n'

    with open(fileName, "wt") as f:
        f.write('Straight skeleton graph G(V, E), where:\n')
        f.write('V = {\n')
        map(lambda v: f.write(vertexPattern % (v[px], v[py])), V)
        f.write('}\n')
        f.write('E = {\n')
        map(lambda edge: f.write(edgePattern % (edge[0][px], edge[0][py], edge[1][px], edge[1][py])), E)
        f.write('}\n')

def main():
    # Read the input polygon from inFileName
    # Anytype of numbers, CCW and convex
    inFileName  = 'in.txt'
    if len(sys.argv) > 1:
        inFileName = sys.argv[1]

    poly = readPoly(inFileName)
    ss   = skeleton.straightSkeleton(poly)

    outFileName = 'out.txt'
    dumpSkeletonGraph(outFileName, ss)

    displaySkeleton(ss, poly)

if __name__ == '__main__':
    main()
