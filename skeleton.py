#-------------------------------------------------------------------------------
# Name:        skeleton.py
# Purpose:     This module contains computation algorithm for a straight skeleton
#              of a convex polygon implementation in a O(NlogN) time, where N
#              is a number of polygon vertices
#
#
# Author:      alexey.vinogradov
#
# Created:     13.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from circularlist import CircularList
from geo import *
from priorityqueue import PriorityQueue


#-------------------------------------------------------------------------------
# A bit about algorithm:
# Theory, idea and description is taken from:
# http://cccg.ca/proceedings/2010/paper55.pdf
# http://www.sthu.org/research/publications/files/phdthesis.pdf
#
# According to given articles, following types are used:
# Edge event is represented via class, that contains following fields:
# Where:
# @point is an intersection point of a rays, formed by the @vertex and @nextVertex
# positions and bisectors
# @vertex is an iterator of a polygon vertex
# @nextVertex is an iterator to the vertex, that follows @vertex
# @length is the distance between the intersection point and the line, formed by
# the right edge's pivot point and the edge direction itself
# @iterator Event iterator in the list
# @prev Previous neighbour event iterator
# @next Next neighbour event iterator
#-------------------------------------------------------------------------------

class EdgeEvent:
    def __init__(self, p = (0.0, 0.0), vi = None, nvi = None, length = 0.0, it = None, pit = None, nit = None):
        self.point       = p
        self.vertex      = vi
        self.nextVertex  = nvi
        self.length      = length
        self.iterator    = it
        self.prev        = pit
        self.next        = nit

    def __str__(self):
        return 'Point: %s, Vertex index: %s, Next vertex index: %s, Length: %s, Index: %s Prev. index: %s, Next index: %s' % (
            self.point,
            self.vertex.value(),
            self.nextVertex.value(),
            self.length,
            self.iterator.value(),
            self.prev.value(),
            self.next.value()
        )

#-------------------------------------------------------------------------------
# Algorithms implementation
#-------------------------------------------------------------------------------

# Generate events queue and edge events queue
def generateEdgeEvent(v0, v1):
    # Obtain intersection of rays using vertex position and bisector
    # and if it exists create edge event
    vertex0 = v0.value()
    vertex1 = v1.value()
    r0 = (vertex0[vpnt], vertex0[vbsr])
    r1 = (vertex1[vpnt], vertex1[vbsr])

    (isects, p) = rayIntersection(r0, r1)
    if isects == True:
        event = EdgeEvent(
            p,
            v0,
            v1,
            pdistance2Line(p, vertex0[vre][epivot], padd(vertex0[vre][epivot], vertex0[vre][edir]))
        )
        return event
    return None


#
# The very first step - initialization, that takes O(NlogN) time
# apply method returns tuple, containing:
# @eventsList - List of all events, that were generated, and it may contain
# None elemenent
# @eventsQueue - PriorityQueue instance, that contains edge events, sorted
# by the @lenght of EdgeEvent
# @verts - Vertices of the polygon, with the neighbour edges set up and
#          bisectors computed
#
class Initialize_:

    def apply(self, poly):
        n    = len(poly[ppnts])

        verts = CircularList()
        for i in range(0, n):
            bis = pdiv(pminus(pnormalize(poly[pedges][i][edir]), pnormalize(poly[pedges][(i + n - 1) % n][edir])), 2.0)
            # Skip points, located on the single line
            if (plen2(bis) >= 0.0):
                verts.pushBack((poly[ppnts][i],
                                bis,
                                poly[pedges][(i + n - 1) % n],
                                poly[pedges][i]))


        def eventsGenerator(verts):
            for v in verts:
                yield generateEdgeEvent(v.clone(), v.getNext())

        # Generate events and push the to the queue
        events = CircularList()
        for e in eventsGenerator(verts):
            events.pushBack(e)

        for e in events:
            event = e.value()
            if event != None:
                event.iterator = e.clone()
                event.prev     = e.getPrev()
                event.next     = e.getNext()

        eventsQueue = PriorityQueue()
        map(lambda e: eventsQueue.push(e.value(), e.value().length) if e.value() != None else None, events)

        return (events,
                eventsQueue,
                verts)

#
# Processing algorithm, that handles all collected in the queue events
# Takes O(NlogN) time, cause total number of events, that are present in the
# queue is N and each iteration 2 more events can be added. Add operation
# costs logN time.
#
class EventProcessor_:

        def apply(self, events):
            evList, evQueue, verts = events

            skeleton = []

            def addBone(p0, p1):
                if plen2(pminus(p0, p1)):
                    skeleton.append((p0, p1))

            while True:
                event = evQueue.pop()

                # Heap returns None, in case everything was removed
                if event == None:
                    break

                v0 = event.vertex.value()
                v1 = event.nextVertex.value()

                # Peak of the roof
                if event.vertex.getPrev().getPrev() == event.nextVertex:
                    addBone(event.point, v0[vpnt])
                    addBone(event.point, v1[vpnt])
                    addBone(event.point, event.vertex.getPrev().value()[vpnt])
                    break

                # Store two more edges
                addBone(event.point, v0[vpnt])
                addBone(event.point, v1[vpnt])

                # Perform split event:
                # 1. Create new vertex
                v = (event.point,
                     pdiv(pminus(pnormalize(v1[vre][edir]), pnormalize(v0[vle][edir])), 2.0),
                     v0[vle],
                     v1[vre]
                )

                # 2. Remove old ones
                where = event.vertex.getPrev()
                verts.erase(event.vertex)
                verts.erase(event.nextVertex)

                # 3. Add new one
                currentVertex = verts.insert(where, v)

                # 4. Remove events - from the list and from the queue
                prevEvent = event.prev.value()
                if prevEvent != None:
                    evQueue.remove(prevEvent)
                nextEvent = event.next.value()
                if nextEvent != None:
                    evQueue.remove(nextEvent)

                # Cache data about pre-previous event and previous vertex -
                # they'll be used to create new one
                prevPrevEvent = event.prev.getPrev()
                prevVertex    = event.vertex.getPrev()

                evList.erase(event.prev)
                evList.erase(event.iterator)
                evList.erase(event.next)

                # 5. Create new events
                newPrevEvent = generateEdgeEvent(prevVertex, prevVertex.getNext())
                newEvent     = generateEdgeEvent(currentVertex, currentVertex.getNext())

                # 6. Add new events
                newPrevEventPos = evList.insert(prevPrevEvent, newPrevEvent)
                newEventPos     = evList.insert(newPrevEventPos, newEvent)

                # 7. Update neighbour events
                newPrevEventPosPrev = newPrevEventPos.getPrev()
                if newPrevEventPosPrev.value() != None:
                    newPrevEventPosPrev.value().next = newPrevEventPos
                newEventPosNext = newEventPos.getNext()
                if newEventPosNext.value() != None:
                    newEventPosNext.value().prev = newEventPos

                if newPrevEvent != None:
                    newPrevEvent.prev     = newPrevEventPos.getPrev()
                    newPrevEvent.iterator = newPrevEventPos.clone()
                    newPrevEvent.next     = newPrevEventPos.getNext()
                if newEvent != None:
                    newEvent.prev     = newEventPos.getPrev()
                    newEvent.iterator = newEventPos.clone()
                    newEvent.next     = newEventPos.getNext()

                # 8. Add new event to the queue
                if newPrevEvent != None:
                    evQueue.push(newPrevEvent, newPrevEvent.length)
                if newEvent != None:
                    evQueue.push(newEvent, newEvent.length)

            return skeleton

#
# Collection of all implemented methods applied in the right order
#
class SkeletonAlgorithm_:
    def __init__(self):
        self.steps_ = [Initialize_(), EventProcessor_()]

    def apply(self, poly):
        res = None
        return self.steps_[1].apply(self.steps_[0].apply(poly))

#
# Export method to launch Straight skeleton computation
#
def straightSkeleton(poly):
    return SkeletonAlgorithm_().apply(poly)
