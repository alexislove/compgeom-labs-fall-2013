#-------------------------------------------------------------------------------
# Name:        geo.py
# Purpose:     Simple geometry operations and data types description
#
# Author:      alexey.vinogradov
#
# Created:     16.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from math import sqrt

#-------------------------------------------------------------------------------
# A bit about data types:
# Point is represented via tuple (x, y)
# Edge is represented via tuple (direction, pivot)
# Vertex is represented via tuple (point, bisector, leftEdge, rightEdge)
# Polygon is represented via tuple (points, edges)
# Ray is represented via tuple (origin, direction)
#
# Scalar is floating point value, due to irrational values, that can be met
# during computation process
#-------------------------------------------------------------------------------
px = 0
py = 1

edir   = 0
epivot = 1

vpnt = 0
vbsr = 1
vle  = 2
vre  = 3

ppnts  = 0
pedges = 1

rorg = 0
rdir = 1

epsilon = 1e-12

#-------------------------------------------------------------------------------
# Point arithmetic
#-------------------------------------------------------------------------------

def padd(a, b):
    return (a[px] + b[px], a[py] + b[py])

def pminus(a, b):
    return (a[px] - b[px], a[py] - b[py])

def pdiv(a, s):
    assert(s != 0.0)
    return (a[px] / s, a[py] / s)

def pdot(a, b):
    return a[px] * b[px] + a[py] * b[py]

def plen(a):
    return sqrt(pdot(a, a))

def plen2(a):
    return pdot(a, a)

def pnormalize(a):
    return pdiv(a, plen(a))

def pangle(a, b):
    return a[px] * b[py] - a[py] * b[px]

def pdistance2Line(p, a, b):
    c  = pangle(pminus(p, a), pminus(p, b))
    # Avoid extra square root calculation
    c2 = c * c
    return c2 / plen2(pminus(a, b))

# Rays intersection computation, returns tuple (bool, point)
def rayIntersection(r0, r1):
    # Parallel rays
    if abs(pangle(r0[rdir], r1[rdir])) < epsilon:
        return (False, (0.0, 0.0))

    o0 = r0[rorg]
    d0 = padd(r0[rorg], r0[rdir])

    o1 = r1[rorg]
    d1 = padd(r1[rorg], r1[rdir])

    denom = 1.0 / ((o0[px] - d0[px]) * (o1[py] - d1[py]) - (o0[py] - d0[py]) * (o1[px] - d1[px]))
    r = (
         (pangle(o0, d0) * (o1[px] - d1[px]) - (o0[px] - d0[px]) * pangle(o1, d1)) * denom,
         (pangle(o0, d0) * (o1[py] - d1[py]) - (o0[py] - d0[py]) * pangle(o1, d1)) * denom
        )

    dr0 = pminus(r, o0)
    dr1 = pminus(r, o1)

    return (((pdot(dr0, r0[rdir]) > 0.0) and (pdot(dr1, r1[rdir]) > 0.0)), r)
