#-------------------------------------------------------------------------------
# Name:        launcher.py
# Purpose:     Straight skeleton builder algorithm launcher
#
# Author:      alexey.vinogradov
#
# Created:     16.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import sys
from geo import *
import skeleton

#
# Read polygon input data from the file with given name
# Takes O(N) time, where N is the number of polygon points
#
def readPoly(fileName):
    with open(fileName) as f:
        # Obtain the list of points, but don't validate the input.
        # If you're wrong, wait for the exception here!
        pts = [(float(line.replace('\n', '').split(' ')[px]), float(line.replace('\n', '').split(' ')[py])) for line in f.readlines()]

        n = len(pts)
        # Setup polygon edges, see above (dir, pivot)
        edges = [((pminus(pts[(i + 1) % n], pts[i])), pts[i]) for i in range(0, n)]

        # Poly is ready...
        return (pts, edges)

#
# Write skeleton into the file with given name
# Takes O(N) time
#
def writeSkeleton(fileName, skeleton):
    pattern = '[(%s, %s) (%s, %s)]\n'
    with open(fileName, "wt") as f:
        map(lambda edge: f.write(pattern % (edge[0][px], edge[0][py], edge[1][px], edge[1][py])), skeleton)


def main():
    # Read the input polygon from inFileName
    # Anytype of numbers, CCW and convex
    inFileName  = 'in.txt'
    if len(sys.argv) > 1:
        inFileName = sys.argv[1]

    outFileName = 'out.txt'
    writeSkeleton(outFileName, skeleton.straightSkeleton(readPoly(inFileName)))

if __name__ == '__main__':
    main()
