#-------------------------------------------------------------------------------
# Name:        priorityqueue.py
# Purpose:     This module contains priority queue implementation as a wrapper
#              around Python's built-in heapq module.
#
# Author:      alexey.vinogradov
#
# Created:     14.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from heapq import heappush, heappop
import itertools

class PriorityQueue:
    def __init__(self):
        self.heap_        = []
        self.entryMap_    = {}
        self.REMOVEDTAG_  = '<removed-entry>'
        self.uniqueCount_ = itertools.count()

    def __str__(self):
        return str(self.heap_)

    def push(self, entry, priority = 0):

        if entry in self.entryMap_:
            self.remove(entry)

        uniqueCount = next(self.uniqueCount_)
        ready       = [priority, uniqueCount, entry]
        self.entryMap_[entry] = ready
        heappush(self.heap_, ready)

    def remove(self, entry):
        # Place tag on the existing item.
        # KeyError will be raised in case item not found
        existing     = self.entryMap_.pop(entry)
        existing[-1] = self.REMOVEDTAG_

    def pop(self):
        # Remove and return lowest priority task
        while self.heap_:
            priority, count, entry = heappop(self.heap_)
            if entry is not self.REMOVEDTAG_:
                del self.entryMap_[entry]
                return entry
