#-------------------------------------------------------------------------------
# Name:        circularlist.py
# Purpose:     This module contains circular list implementation,that grants
#              O(1) insert and remove operation time, unlike Python's builtin
#              list. Also list provides C++ STL-alike iterators.
#
# Author:      alexey.vinogradov
#
# Created:     15.12.2013
# Copyright:   (c) alexey.vinogradov 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

#
# Basic double-linked list node
#
class Node_:

    def __init__(self, value = None, next = None, prev = None):
        self.value = value
        self.next  = self if next == None else next
        self.prev  = self if prev == None else prev


class CircularList:

    class Iterator:
        def __init__(self, theList, node = None):
            self.list_    = theList
            self.current_ = self.list_.head_ if node == None else node

        def __eq__(self, rhs):
            if isinstance(rhs, self.__class__):
                if self.list_ != rhs.list_:
                    raise TypeError('Iterators belong to the different lists!')
                return self.current_ == rhs.current_
            return NotImplemented

        def __ne__(self, rhs):
            if isinstance(rhs, self.__class__):
                if self.list_ != rhs.list_:
                    raise TypeError('Iterators belong to the different lists!')
                return self.current_ != rhs.current_
            return NotImplemented

        def __iter__(self):
            return self

        def value(self):
            return self.current_.value

        def next(self):
            self.current_ = self.current_.next
            if self.current_ == self.list_.head_:
                raise StopIteration
            return self

        def prev(self):
            self.current_ = self.current_.prev
            if self.current_ == self.list_.head_:
                raise StopIteration
            return self

        def clone(self):
            return self.__class__(self.list_, self.current_)

        def getNext(self):
            next = self.__class__(self.list_, self.current_.next)
            # Make list circular here!
            if next.current_ == self.list_.head_:
                next.current_ = next.current_.next
            return next

        def getPrev(self):
            prev = self.__class__(self.list_, self.current_.prev)
            # Make list circular here!
            if prev.current_ == self.list_.head_:
                prev.current_ = prev.current_.prev
            return prev


    def __init__(self):
        self.head_ = Node_()
        self.size_ = 0

    def __iter__(self):
        # Return non-begin iterator, because next will be called right after
        # iterator is obtained in for * in MyList construction
        return self.Iterator(self, self.head_)

    #
    # Insert 'value' after 'where'
    # Returns iterator, pointing on the inserted value
    #
    def insert(self, where, value):
        whereNode = where.current_
        newNode   = Node_(value, whereNode.next, whereNode)
        # Setup where node successors and predecessors
        whereNode.next.prev = newNode
        whereNode.next      = newNode

        self.size_ += 1

        return self.Iterator(self, newNode)

    #
    # Insert 'value' to the list beginning
    #
    def pushFront(self, value):
        self.insert(self.Iterator(self, self.head_), value)

    #
    # Insert 'value' to the list end
    #
    def pushBack(self, value):
        self.insert(self.Iterator(self, self.head_.prev), value)

    #
    # Erase 'value' from the list
    #
    def erase(self, where):
        whereNode = where.current_
        if whereNode == self.head_ or self.size_ == 0:
            raise IndexError('Erase iterator out of bounds!')

        # Setup predecessor and successor
        whereNode.prev.next = whereNode.next
        whereNode.next.prev = whereNode.prev

        self.size_ -= 1

        # Keep memory tight
        del whereNode

    #
    # Clear the list
    #
    def clear(self):
        while not self.isEmpty():
            self.erase(begin())

    #
    # Get current list size
    #
    def size(self):
        return self.size_

    #
    # Check, whether list is empty or not
    #
    def isEmpty(self):
        return self.size_ == 0

    #
    # Get iterator to the list beginning
    #
    def begin(self):
        return self.Iterator(self, self.head_.next)

    #
    # Get iterator to the list ending
    #
    def end(self):
        # Head node is asummed to be the list ending
        return self.Iterator(self, self.head_)
